package io.reconnect;

public enum Duration {

    D_1_MIN("1", "minute", 1),
    D_15_MIN("15", "minutes", 15),
    D_30_MIN("30", "minutes", 30),
    D_1_HOUR("1", "hour", 60),
    D_2_HOURS("2", "hours", 120),
    D_4_HOURS("4", "hours", 240),
    D_8_HOURS("8", "hours", 480),
    D_HALF_DAY("half", "a day", 720),
    D_1_DAY("1", "day", 1440);

    private Duration(String value, String label, Integer minutes) {
        this.value = value;
        this.label = label;
        this.minutes = minutes;
    }

    private String value;
    private String label;
    private Integer minutes;

    public String getValue() {
        return value;
    }

    public String getLabel() {
        return label;
    }

    public Integer getMinutes() {
        return minutes;
    }

    public long getMillis() {
        return minutes * 60000;
    }

    public static Duration next(Duration duration){
        Duration[] durations = Duration.values();
        for(int i = 0; i < durations.length; i++){
            if(durations[i].equals(duration)){
                if(i == durations.length - 1){
                    return durations[0];
                }
                return durations[i + 1];
            }
        }
        return Duration.D_1_MIN;
    }

    public static Duration previous(Duration duration){
        Duration[] durations = Duration.values();
        for(int i = 0; i < durations.length; i++){
            if(durations[i].equals(duration)){
                if(i == 0){
                    return durations[durations.length - 1];
                }
                return durations[i - 1];
            }
        }
        return Duration.D_1_MIN;
    }
}
