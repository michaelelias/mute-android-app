package io.reconnect.util;

import android.content.Context;
import android.graphics.Typeface;
import android.widget.TextView;

public class FontUtils {

    public static final String ROBOTO_REGULAR = "Roboto-Regular.ttf";
    public static final String ROBOTO_LIGHT = "Roboto-Light.ttf";
    public static final String ROBOTO_BLACK = "Roboto-Black.ttf";
    public static final String ROBOTO_BOLD = "Roboto-Bold.ttf";
    public static final String ROBOTO_MEDIUM = "Roboto-Medium.ttf";
    public static final String ROBOTO_THIN = "Roboto-Thin.ttf";
    private Context ctx;

    public FontUtils(Context ctx) {
        this.ctx = ctx;
    }

    public void setRobotoLight(TextView view){
        setFont(view, ROBOTO_LIGHT);
    }

    public void setRoboto(TextView view){
        setFont(view, ROBOTO_REGULAR);
    }

    private void setFont(TextView view, String fontName){
        Typeface typeface = Typeface.createFromAsset(ctx.getAssets(), "fonts/" + ROBOTO_LIGHT);
        view.setTypeface(typeface);
    }

//    public void setRoboto(ViewGroup viewGroup){
//
//    }

}
