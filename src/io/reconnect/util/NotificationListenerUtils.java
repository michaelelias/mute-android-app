package io.reconnect.util;

import android.app.ActivityManager;
import android.content.Context;
import android.content.pm.PackageManager;
import io.reconnect.NotificationListener;

public class NotificationListenerUtils {

    public static boolean hasNotificationListenerPermission(Context ctx){
        String permission = "android.permission.BIND_NOTIFICATION_LISTENER_SERVICE";
        int res = ctx.checkCallingOrSelfPermission(permission);
        return (res == PackageManager.PERMISSION_GRANTED);
//        PackageManager packageManager = activity.getPackageManager();
//        return packageManager.checkPermission(Manifest.permission.BIND_NOTIFICATION_LISTENER_SERVICE, activity.getPackageName()) == PackageManager.PERMISSION_GRANTED;
    }

    public static boolean isNotificationListenerRunning(Context ctx) {
        ActivityManager manager = (ActivityManager) ctx.getSystemService(Context.ACTIVITY_SERVICE);

        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (NotificationListener.class.getName().equals(service.service.getClassName())) {
                return true;
            }
        }

        return false;

    }

}
