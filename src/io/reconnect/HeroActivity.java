package io.reconnect;

import android.app.ActionBar;
import android.app.Activity;
import android.app.NotificationManager;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import io.reconnect.util.FontUtils;

import java.util.ArrayList;
import java.util.List;

public class HeroActivity extends Activity {

    private static final List<String> messages = new ArrayList<String>();

    static {
        messages.add("reconnect for...");
        messages.add("be social for...");
        messages.add("hang out with friends for...");
        messages.add("walk the dog for...");
        messages.add("meditate for...");
        messages.add("do some yoga for...");
        messages.add("watch a movie for...");
        messages.add("have family time for...");
        messages.add("play board games for...");
        messages.add("bake something for...");
        messages.add("learn something new for...");
    }

    private boolean muted = false;
    FontUtils fontUtils;

    private Duration duration = Duration.D_1_MIN;
    private int messageIndex = 0;

    TextView txtTitle;
    TextView txtDurationValue;
    TextView txtDurationLabel;
    private ViewSwitcher viewSwitcher;
    private Button btnStop;
    private TextView txtActiveRemainingTime;
    private ProgressBar progressActive;
    private Button btnSimulate;
    private int testNotificationCount = 0;

    private Controller controller;
    private Model model;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fontUtils = new FontUtils(this);
        model = new Model();
        controller = new Controller(this, model);

        initActionBar();
        initView();
        bindConfigView();
    }

    private void initActionBar() {
        ActionBar actionBar = getActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setTitle(R.string.app_name);
    }

    private void initView() {
        viewSwitcher = new ViewSwitcher(this);
        ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        viewSwitcher.setLayoutParams(params);
        setContentView(viewSwitcher);
        getLayoutInflater().inflate(R.layout.set_mute, viewSwitcher);
        getLayoutInflater().inflate(R.layout.active_mute, viewSwitcher);

        initConfigView(viewSwitcher.getChildAt(0));
        initActiveView(viewSwitcher.getChildAt(1));

        model.registerListener(new Model.Listener() {
            @Override
            public void onStart(Model model) {
                bindActiveView();
                viewSwitcher.setDisplayedChild(1);
            }

            @Override
            public void onStop(Model model) {
                // Show config screen
                viewSwitcher.setDisplayedChild(0);
            }

            @Override
            public void onTick(Model model) {
                bindActiveView();
            }

            @Override
            public void onFinish(Model model) {
                // Show config screen
                viewSwitcher.setDisplayedChild(0);
            }
        });
    }

    private void initConfigView(View view) {
        txtTitle = (TextView) view.findViewById(R.id.txt_title);
        fontUtils.setRoboto(txtTitle);

        txtDurationValue = (TextView) view.findViewById(R.id.txt_duration_value);
        fontUtils.setRoboto(txtDurationValue);

        txtDurationLabel = (TextView) view.findViewById(R.id.txt_duration_label);
        fontUtils.setRobotoLight(txtDurationLabel);

        Button btnStart = (Button) view.findViewById(R.id.btn_start);
        fontUtils.setRobotoLight(btnStart);
        btnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                controller.startMute(duration);
            }
        });

        ImageButton btnUp = (ImageButton) view.findViewById(R.id.btn_up);
        btnUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                duration = Duration.previous(duration);
                bindConfigView();
            }
        });

        ImageButton btnDown = (ImageButton) view.findViewById(R.id.btn_down);
        btnDown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                duration = Duration.next(duration);
                bindConfigView();
            }
        });
    }

    private void initActiveView(View view) {
        txtActiveRemainingTime = (TextView) view.findViewById(R.id.txt_active_remaining_time);
        progressActive = (ProgressBar) view.findViewById(R.id.progress_active);
        btnStop = (Button) view.findViewById(R.id.btn_active_stop);
        btnSimulate = (Button) view.findViewById(R.id.btn_simulate);

        btnStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                controller.stopMute();
            }
        });

        btnSimulate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendTestNotification();
            }
        });
    }

    private void bindConfigView() {
        txtTitle.setText(nextMessage());
        txtDurationValue.setText(duration.getValue());
        txtDurationLabel.setText(duration.getLabel());
    }

    private void bindActiveView() {
        Model model = controller.getModel();
        txtActiveRemainingTime.setText(model.getRemainingTimeFormatted());
        progressActive.setMax(model.getDurationTime());
        progressActive.setProgress(model.getExpiredTime());
    }

    private void sendTestNotification(){
        NotificationManager nManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        testNotificationCount++;
        NotificationCompat.Builder ncomp = new NotificationCompat.Builder(this);
        ncomp.setContentTitle("Test Notification " + testNotificationCount);
        ncomp.setContentText("Notification Listener Service Example");
        ncomp.setTicker("Notification Listener Service Example");
        ncomp.setSmallIcon(R.drawable.ic_launcher_reconnect);
        ncomp.setAutoCancel(true);
        nManager.notify((int) System.currentTimeMillis(), ncomp.build());
    }



    @Override
    protected void onDestroy() {
        super.onDestroy();
        controller.destroy();
    }

    private String nextMessage() {
        if (messageIndex == messages.size() - 1) {
            messageIndex = 0;
        } else {
            messageIndex++;
        }
        return messages.get(messageIndex);
    }

}
