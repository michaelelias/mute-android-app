package io.reconnect;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.service.notification.StatusBarNotification;
import android.util.Log;
import io.reconnect.util.NotificationListenerUtils;

import java.util.List;

public class Controller {

    private static final String TAG = "Reconnect-" + Controller.class.getSimpleName();

    private HeroActivity activity;
    private Model model;
    private BroadcastManager broadcastMgr;
    private NotificationReceiver receiver;
    private PermissionCheckResultCallback permissionCheckCallback;

    public Controller(HeroActivity activity, Model model) {
        this.activity = activity;
        this.model = model;
        broadcastMgr = new BroadcastManager(activity);
        receiver = new NotificationReceiver();
        broadcastMgr.registerReceiver(receiver);
    }

    public Model getModel() {
        return model;
    }

    public void destroy() {
        broadcastMgr.unregisterReceiver(receiver);
    }


    public void startMute(final Duration duration) {
//        // check if the notification service is running
//        if(!NotificationListenerUtils.isNotificationListenerRunning(activity)){
//            // try to start the service
//            activity.startService(new Intent(activity, NotificationListener.class));
//            if(!NotificationListenerUtils.isNotificationListenerRunning(activity)){
//                // something went wrong
//                Log.i(TAG, "Could not start notification listener service");
//                return;
//            }
//        }

        // check if the notification listener is allowed to capture notification.
//        hasNotificationListenerPermission(new PermissionCheckResultCallback() {
//            @Override
//            public void onResult(boolean hasPermission) {
//                if(!hasPermission){
//                    // Drop user off at notification listener settings
//                    activity.startActivity(new Intent("android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS"));
//                    return;
//                }
//
//                model.setDuration(duration);
//                model.start();
//                broadcastMgr.broadcastMutedChanged(true);
//            }
//        });

        if(!NotificationListener.isNotificationAccessEnabled()){
            // Drop user off at notification listener settings
            activity.startActivity(new Intent("android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS"));
            return;
        }

        model.setDuration(duration);
        model.start();
        broadcastMgr.broadcastMutedChanged(true);
    }

    private void hasNotificationListenerPermission(PermissionCheckResultCallback callback){
        this.permissionCheckCallback = callback;
        broadcastMgr.broadcastPermissionCheck();
    }


    public void stopMute(){
        model.stop();
        // Turn off notification capture
        broadcastMgr.broadcastMutedChanged(false);
    }

    private class NotificationReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (broadcastMgr.isCapturedNotificationIntent(intent)) {
                model.addCapturedNotification(broadcastMgr.extractCapturedNotification(intent));
            }
            else if(broadcastMgr.isMutedIntent(intent)){
                if(!broadcastMgr.extractMuted(intent)){
                    // Replay all captured notifications
                    NotificationManager notificationManager = (NotificationManager) activity.getSystemService(Context.NOTIFICATION_SERVICE);
                    List<CapturedNotification> capturedNotifications = model.getCapturedNotifications();
                    for (CapturedNotification capturedNotification : capturedNotifications) {
                        StatusBarNotification statusBarNotification = capturedNotification.getStatusBarNotification();
                        notificationManager.notify(statusBarNotification.getId(), statusBarNotification.getNotification());
                    }
                    capturedNotifications.clear();
                }

            }
            else if(broadcastMgr.isPermissionResultIntent(intent)){
                permissionCheckCallback.onResult(broadcastMgr.extractPermissionResult(intent));
            }
        }
    }

    public interface PermissionCheckResultCallback {

        public void onResult(boolean hasPermission);

    }

}
