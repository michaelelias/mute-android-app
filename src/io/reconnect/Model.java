package io.reconnect;

import android.os.CountDownTimer;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Contains the details of an active mute operation.
 */
public class Model {

    private static final long HOUR = 60 * 60 * 1000;
    private static final long MINUTE = 60 * 1000;

    private Duration duration;
    private Long startTime;
    private boolean muted;

    private List<CapturedNotification> capturedNotifications = new ArrayList<CapturedNotification>();
    private List<Listener> listeners = new ArrayList<Listener>();
    private CountDownTimer countDownTimer;

    public Model(Listener... listeners) {
        for (Listener listener : listeners) {
            registerListener(listener);
        }
    }

    public void registerListener(Listener listener){
        this.listeners.add(listener);
    }

    public String getRemainingTimeFormatted() {
        long currentTime = System.currentTimeMillis();
        long durationInMillis = duration.getMillis();
        long expiredTime = currentTime - startTime;
        long remainingTime = durationInMillis - expiredTime;

        long hours = remainingTime / HOUR;
        long remainingTimeMinusHours = (remainingTime % HOUR);
        long minutes = remainingTimeMinusHours / MINUTE + (remainingTimeMinusHours % MINUTE > 0 ? 1 : 0);

        return hours + "h " + minutes + "m";
    }

    public boolean isMuted() {
        return muted;
    }

    public void setDuration(Duration duration) {
        this.duration = duration;
    }

    public Integer getDurationTime() {
        return (int) duration.getMillis();
    }

    public Integer getExpiredTime() {
        long currentTime = System.currentTimeMillis();
        return (int) (currentTime - startTime);
    }

    public void start() {
        muted = true;
        startTime = System.currentTimeMillis();
        long durationInMillis = duration.getMillis();
        countDownTimer = new CountDownTimer(startTime + durationInMillis, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                for(Listener listener: listeners){
                    listener.onTick(Model.this);
                }
            }

            @Override
            public void onFinish() {
                muted = false;
                for(Listener listener: listeners){
                    listener.onFinish(Model.this);
                }

            }
        };
        countDownTimer.start();
        for(Listener listener: listeners){
            listener.onStart(this);
        }
    }

    public void stop() {
        muted = false;
        countDownTimer.cancel();
        for(Listener listener: listeners){
            listener.onStop(this);
        }
    }

    public void clear(){
        capturedNotifications.clear();
    }

    public void addCapturedNotification(CapturedNotification capturedNotification) {
        if (capturedNotification.getType().equals(BroadcastManager.StatusBarNotificationType.ADDED)) {
            capturedNotifications.add(capturedNotification);
        } else {
            Iterator<CapturedNotification> iterator = capturedNotifications.iterator();
            while (iterator.hasNext()) {
                CapturedNotification notification = iterator.next();
                if (notification.toString().equals(capturedNotification.toString())) {
                    iterator.remove();
                    break;
                }
            }
        }
    }

    public List<CapturedNotification> getCapturedNotifications() {
        return capturedNotifications;
    }

    public interface Listener {

        void onStart(Model model);

        void onStop(Model model);

        void onTick(Model model);

        void onFinish(Model model);
    }

}
