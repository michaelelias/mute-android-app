package io.reconnect;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.service.notification.StatusBarNotification;
import android.util.Log;

public class BroadcastManager {

    private static final String TAG = "Reconnect-" + BroadcastManager.class.getSimpleName();

    public static final String INTENT_ACTION = "io.reconnect";

    public static final String STATUS_BAR_NOTIFICATION = "StatusBarNotification";
    public static final String STATUS_BAR_NOTIFICATION_TYPE = "StatusBarNotificationType";

    public static final String MUTED = "Muted";
    public static final String PERMISSION_CHECK = "PermissionCheck";
    public static final String PERMISSION_RESULT = "PermissionResult";

    public boolean muted;

    public Context ctx;

    public BroadcastManager(Context ctx) {
        this.ctx = ctx;
    }

    public boolean isMuted() {
        return muted;
    }

    public boolean isCapturedNotificationIntent(Intent intent){
        return intent.hasExtra(STATUS_BAR_NOTIFICATION);
    }

    public boolean isMutedIntent(Intent intent){
        return intent.hasExtra(MUTED);
    }

    public boolean isPermissionCheckIntent(Intent intent){
        return intent.hasExtra(PERMISSION_CHECK);
    }

    public boolean isPermissionResultIntent(Intent intent){
        return intent.hasExtra(PERMISSION_RESULT);
    }

    public CapturedNotification extractCapturedNotification(Intent intent){
        StatusBarNotification statusBarNotification = intent.getParcelableExtra(STATUS_BAR_NOTIFICATION);
        String type = intent.getStringExtra(STATUS_BAR_NOTIFICATION_TYPE);
        return new CapturedNotification(statusBarNotification, StatusBarNotificationType.valueOf(type));
    }

    public boolean extractMuted(Intent intent){
        return intent.getBooleanExtra(MUTED, false);
    }

    public boolean extractPermissionResult(Intent intent){
        return intent.getBooleanExtra(PERMISSION_RESULT, false);
    }

    public void broadcastCapturedNotification(StatusBarNotification statusBarNotification, StatusBarNotificationType type){
        Intent intent = new Intent(INTENT_ACTION);
        intent.putExtra(STATUS_BAR_NOTIFICATION, statusBarNotification);
        intent.putExtra(STATUS_BAR_NOTIFICATION_TYPE, type.name());
        ctx.sendBroadcast(intent);
        Log.d(TAG, "Broadcasting captured notification");
    }

    public void broadcastMutedChanged(boolean muted){
        Intent intent = new Intent(INTENT_ACTION);
        intent.putExtra(MUTED, muted);
        ctx.sendBroadcast(intent);
        Log.d(TAG, "Broadcasting muted has changed to " + muted);
    }

    public void broadcastPermissionCheck(){
        Intent intent = new Intent(INTENT_ACTION);
        intent.putExtra(PERMISSION_CHECK, true);
        ctx.sendBroadcast(intent);
        Log.d(TAG, "Broadcasting permission check");
    }

    public void broadcastPermissionCheckResult(boolean hasPermission){
        Intent intent = new Intent(INTENT_ACTION);
        intent.putExtra(PERMISSION_RESULT, hasPermission);
        ctx.sendBroadcast(intent);
        Log.d(TAG, "Broadcasting permission check result : " + hasPermission);
    }

    public void registerReceiver(BroadcastReceiver receiver) {
        IntentFilter filter = new IntentFilter();
        filter.addAction(INTENT_ACTION);
        ctx.registerReceiver(receiver, filter);
    }

    public void unregisterReceiver(BroadcastReceiver receiver) {
        ctx.unregisterReceiver(receiver);
    }

    public enum StatusBarNotificationType {
        ADDED,
        REMOVED;
    }
}
