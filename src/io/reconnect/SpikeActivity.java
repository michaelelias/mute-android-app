package io.reconnect;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;

import java.util.ArrayList;
import java.util.List;

public class SpikeActivity extends Activity {

    private CheckBox cbxToggleMute;

    private NotificationListAdapter adapter;

    List<CapturedNotification> capturedNotifications = new ArrayList<CapturedNotification>();

    private boolean muted = false;

    NotificationReceiver receiver;

    BroadcastManager broadcastMgr;

    private int testNotificationCount = 0;


    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        broadcastMgr = new BroadcastManager(this);

        cbxToggleMute = (CheckBox) findViewById(R.id.cbx_toggle_mute);
        cbxToggleMute.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                setMuted(isChecked);
            }
        });
        cbxToggleMute.setChecked(muted);

        Button btnSendTestNotification = (Button)findViewById(R.id.btn_test_notification);
        btnSendTestNotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendTestNotification();
            }
        });

        Button btnOpenNotificationSettings = (Button)findViewById(R.id.btn_notification_settings);
        btnOpenNotificationSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent("android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS");
                startActivity(intent);
            }
        });

        adapter = new NotificationListAdapter();
        ListView list = (ListView)findViewById(R.id.list_notifcations);
        list.setAdapter(adapter);

        receiver = new NotificationReceiver();
        broadcastMgr.registerReceiver(receiver);
        broadcastMgr.broadcastMutedChanged(muted);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(receiver);
    }

    private void setMuted(boolean muted){
        this.muted = muted;
        broadcastMgr.broadcastMutedChanged(muted);
    }

    private void sendTestNotification(){
        NotificationManager nManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        testNotificationCount++;
        NotificationCompat.Builder ncomp = new NotificationCompat.Builder(this);
        ncomp.setContentTitle("Test Notification " + testNotificationCount);
        ncomp.setContentText("Notification Listener Service Example");
        ncomp.setTicker("Notification Listener Service Example");
        ncomp.setSmallIcon(R.drawable.ic_launcher_reconnect);
        ncomp.setAutoCancel(true);
        nManager.notify((int)System.currentTimeMillis(),ncomp.build());
    }

    private class NotificationListAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return capturedNotifications.size();
        }

        @Override
        public Object getItem(int position) {
            return capturedNotifications.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            CapturedNotification capturedNotification = (CapturedNotification)getItem(position);

            convertView = capturedNotification.getStatusBarNotification().getNotification().contentView.apply(SpikeActivity.this, parent);
            convertView.getLayoutParams().width = ViewGroup.LayoutParams.MATCH_PARENT;

//            if(convertView == null){
//                convertView = getLayoutInflater().inflate(R.layout.notification_list_item, null);
//            }
//
//            CapturedNotification capturedNotification = (CapturedNotification)getItem(position);
//
//            TextView txtDescription = (TextView)convertView.findViewById(R.id.txt_notification_description);
//
//            txtDescription.setText(capturedNotification.getStatusBarNotification().getNotification().tickerText);

            return convertView;
        }
    }

    private class NotificationReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            if(broadcastMgr.isCapturedNotificationIntent(intent)){
                capturedNotifications.add(broadcastMgr.extractCapturedNotification(intent));
                adapter.notifyDataSetChanged();
            }
        }
    }
}
