package io.reconnect;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.IBinder;
import android.service.notification.NotificationListenerService;
import android.service.notification.StatusBarNotification;
import android.util.Log;
import io.reconnect.util.NotificationListenerUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class NotificationListener extends NotificationListenerService {

    private static final String TAG = "Reconnect-" + NotificationListener.class.getSimpleName();

    private static boolean notificationAccessEnabled = false;

    private BroadcastManager broadcastMgr;

    private boolean muted = false;

    private ServiceReceiver receiver;

    List<StatusBarNotification> notificationsBeingMuted = Collections.synchronizedList(new ArrayList<StatusBarNotification>());

    @Override
    public void onCreate() {
        super.onCreate();
        broadcastMgr = new BroadcastManager(this);

        receiver = new ServiceReceiver();
        broadcastMgr.registerReceiver(receiver);
        Log.i(TAG, "Service Created");
    }

    @Override
    public void onNotificationPosted(StatusBarNotification sbn) {
        Log.i(TAG, "Notification posted " + sbn.getId());
        if (muted) {
            notificationsBeingMuted.add(sbn);
            cancelNotification(sbn.getPackageName(), sbn.getTag(), sbn.getId());
            broadcastMgr.broadcastCapturedNotification(sbn, BroadcastManager.StatusBarNotificationType.ADDED);
        }
    }

    @Override
    public void onNotificationRemoved(StatusBarNotification sbn) {
        Log.i(TAG, "Notification removed " + sbn.getId());

        // check if the removed notification is a muted one and if it is, don't broadcast a capture notification event
        synchronized (notificationsBeingMuted) {
            Iterator<StatusBarNotification> iterator = notificationsBeingMuted.iterator();
            while (iterator.hasNext()) {
                StatusBarNotification mutedSbn = iterator.next();
                if (equals(mutedSbn, sbn)) {
                    iterator.remove();
                    return;
                }
            }
        }

        if (muted) {
            cancelNotification(sbn.getPackageName(), sbn.getTag(), sbn.getId());
            broadcastMgr.broadcastCapturedNotification(sbn, BroadcastManager.StatusBarNotificationType.REMOVED);
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public IBinder onBind(Intent mIntent) {
        IBinder mIBinder = super.onBind(mIntent);
        notificationAccessEnabled = true;
        return mIBinder;
    }

    @Override
    public boolean onUnbind(Intent mIntent) {
        boolean mOnUnbind = super.onUnbind(mIntent);
        notificationAccessEnabled = false;
        return mOnUnbind;
    }

    public static boolean isNotificationAccessEnabled() {
        return notificationAccessEnabled;
    }

    private boolean equals(StatusBarNotification sbn1, StatusBarNotification sbn2) {
        if (sbn1.getId() != sbn2.getId()) {
            return false;
        }
        if (sbn1.getPackageName() != sbn2.getPackageName()) {
            return false;
        }
        if (sbn1.getPostTime() != sbn2.getPostTime()) {
            return false;
        }
        if (sbn1.getTag() != sbn2.getTag()) {
            return false;
        }
        return true;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        broadcastMgr.unregisterReceiver(receiver);
        Log.i(TAG, "Service Destroyed");
    }

    class ServiceReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (broadcastMgr.isMutedIntent(intent)) {
                muted = broadcastMgr.extractMuted(intent);
                Log.i(TAG, "Received muted changed broadcast : " + muted);
            } else if (broadcastMgr.isPermissionCheckIntent(intent)) {
                Log.i(TAG, "Received permission check broadcast");
                boolean hasPermission = NotificationListenerUtils.hasNotificationListenerPermission(NotificationListener.this);
                broadcastMgr.broadcastPermissionCheckResult(hasPermission);
            }

        }
    }


}
