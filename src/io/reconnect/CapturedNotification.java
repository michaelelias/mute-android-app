package io.reconnect;

import android.service.notification.StatusBarNotification;

public class CapturedNotification {

    private StatusBarNotification statusBarNotification;
    private BroadcastManager.StatusBarNotificationType type;

    public CapturedNotification(StatusBarNotification statusBarNotification, BroadcastManager.StatusBarNotificationType type) {
        this.statusBarNotification = statusBarNotification;
        this.type = type;
    }

    public StatusBarNotification getStatusBarNotification() {
        return statusBarNotification;
    }

    public BroadcastManager.StatusBarNotificationType getType() {
        return type;
    }


}
